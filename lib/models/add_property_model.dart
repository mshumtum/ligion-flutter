import 'package:firebase_database/firebase_database.dart';

class AddPropertyModel {
  String ownerContact;
  String ownerName;
  String key;
  String houseNo;
  String houseDescription;
  String streetName;
  String address;
  String _landmark;
  String imgUrl;
  String lat;
  String lng;
  String pin;
  String userId;

  AddPropertyModel(this.ownerContact,this.ownerName, this.houseNo,this.houseDescription, this.streetName,this.address,this._landmark,
      this.imgUrl, this.lat, this.lng, this.pin, this.userId);

  AddPropertyModel.fromSnapshot(DataSnapshot snapshot)
      : key = snapshot.key,
        userId = snapshot.value["userId"],
        ownerContact = snapshot.value["ownerContact"],
        ownerName = snapshot.value["ownerName"],
        houseNo = snapshot.value["houseNo"],
        houseDescription = snapshot.value["houseDescription"],
        streetName = snapshot.value["streetName"],
        address = snapshot.value["address"],
        imgUrl = snapshot.value["imgUrl"],
        lat = snapshot.value["lat"],
        lng = snapshot.value["lng"],
        pin = snapshot.value["pin"];

  toJson() {
    return {
      "userId": userId,
      "ownerContact": ownerContact,
      "ownerName": ownerName,
      "houseNo": houseNo,
      "houseDescription": houseDescription,
      "streetName": streetName,
      "address": address,
      "imgUrl": imgUrl,
      "lat": lat,
      "lng": lng,
      "pin": pin,
    };
  }
}
