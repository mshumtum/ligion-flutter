import 'package:flutter/material.dart';
import 'package:ligion_auth/pages/root_page.dart';
import 'package:ligion_auth/services/authentication.dart';
import 'package:splashscreen/splashscreen.dart';

void main() {
  runApp(new MaterialApp(
    home: new MyApp(),
    theme: new ThemeData(
      primarySwatch: Colors.brown,
    ),
  ));
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppSplash();
  }
}

class _MyAppSplash extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 4,
        navigateAfterSeconds: new RootPage(auth: new Auth()),
        title: new Text(
          'Howell Business',
          style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
        ),
        image: new Image.asset('images/icon.png'),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        onClick: () => print("Flutter Egypt"),
        loaderColor: Colors.brown);
  }
}
