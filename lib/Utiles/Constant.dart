class Constants{
  static const String EDIT="Edit";
  static const String DELETE="Delete";
  static const String UPLOADING="Uploading";
  static const String FILE_UPLOADED="File Uploaded";
  static const String IMAGE_TEXT="Click here to take a Snap!";
  static const String IMAGE_ERR="Sorry! Please try again.";
  static const String IMAGE_UPDATE_TEXT="Click here to update a Snap!";
  static const String PROPERTY_DETAILS="Property Details";
  static const String EDIT_PROPERTY="Edit Property";
  static const List<String> MAIN_POP_UP= <String>[
    EDIT,DELETE
  ];
  static String USER_ID="";
}