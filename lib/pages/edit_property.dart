import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ligion_auth/Utiles/Constant.dart';
import 'package:ligion_auth/models/add_property_model.dart';
import 'package:path/path.dart' as Path;

class EditProperty extends StatefulWidget {
  @override
  _EditPropertyState createState() => _EditPropertyState();
}

class _EditPropertyState extends State<EditProperty> {
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  Query _todoQuery;
  AddPropertyModel _propertyModel;
  String _imgURL = "";
  String imageText = Constants.IMAGE_UPDATE_TEXT;
  String _uploadedFileURL = "";
  File _image;
  bool _isLoading;
  double _progess = 0;
  var _phone = TextEditingController();
  var _ownerInfo = TextEditingController();
  var _houseNo = TextEditingController();
  var _houseDesc = TextEditingController();
  var _pin = TextEditingController();
  var _street = TextEditingController();
  var _address = TextEditingController();
  var _landmark = TextEditingController();

  @override
  void initState() {
    _isLoading = false;
    _todoQuery = _database
        .reference()
        .child("Properties")
        .orderByChild("userId")
        .equalTo(Constants.USER_ID);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _propertyModel = ModalRoute
        .of(context)
        .settings
        .arguments;
    _ownerInfo = TextEditingController(text: _propertyModel.ownerName);
    _phone = TextEditingController(text: _propertyModel.ownerContact);
    _houseNo = TextEditingController(text: _propertyModel.houseNo);
    _houseDesc = TextEditingController(text: _propertyModel.houseDescription);
    _street = TextEditingController(text: _propertyModel.streetName);
    _address = TextEditingController(text: _propertyModel.address);
    _pin = TextEditingController(text: _propertyModel.pin);
    _uploadedFileURL = _propertyModel.imgUrl;
    return Scaffold(
      appBar: AppBar(title: Text(Constants.EDIT_PROPERTY)),
      body: Stack(
        children: <Widget>[
          _showForm(context),
          _showCircularProgress(),
        ],
      ),
    );
  }

  Widget _showForm(BuildContext context) {
    return new Container(
      padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 12.0),
      child: new ListView(
        shrinkWrap: true,
        children: <Widget>[
          showImage(context),
          showImageIcon(),
          showOwnerName(),
          showOwnerContact(),
          showHouseNumber(),
          showHouseDiscription(),
          showStreetName(),
          showAddress(),
          showLandmark(),
          showPinCode(),
          showPrimaryButton()
        ],
      ),
    );
  }

  Widget showImage(BuildContext context) {
    if (_imgURL.length > 0 && _imgURL != null) {
      return new GestureDetector(
          child: new Container(
            child: new Image.file(_image),
            alignment: Alignment.center,
            height: 270.0,
          ));
    } else {
      return new GestureDetector(
          child: new Container(
            child: new Image.network(_propertyModel.imgUrl),
            alignment: Alignment.center,
            height: 270.0,
          ));
    }
  }

  Widget showOwnerName() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Owner Name',
            icon: new Icon(
              Icons.person_pin,
              color: Colors.grey,
            )),
        controller: _ownerInfo,
      ),
    );
  }

  Widget showOwnerContact() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.phone,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Owner Contact Number',
            icon: new Icon(
              Icons.phone,
              color: Colors.grey,
            )),
        controller: _phone,
      ),
    );
  }

  Widget showHouseNumber() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Property Name',
            icon: new Icon(
              Icons.home,
              color: Colors.grey,
            )),
        controller: _houseNo,
      ),
    );
  }

  Widget showHouseDiscription() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Property Description like 3BHK',
            icon: new Icon(
              Icons.home,
              color: Colors.grey,
            )),
        controller: _houseDesc,
      ),
    );
  }

  Widget showStreetName() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Enter Street name',
            icon: new Icon(
              Icons.pin_drop,
              color: Colors.grey,
            )),
        controller: _street,
      ),
    );
  }

  Widget showAddress() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Enter Address',
            icon: new Icon(
              Icons.pin_drop,
              color: Colors.grey,
            )),
        controller: _address,
      ),
    );
  }

  Widget showLandmark() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Landmark',
            icon: new Icon(
              Icons.pin_drop,
              color: Colors.grey,
            )),
        controller: _landmark,
      ),
    );
  }

  Widget showPinCode() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Postal code',
            icon: new Icon(
              Icons.local_post_office,
              color: Colors.grey,
            )),
        controller: _pin,
      ),
    );
  }

  Widget showPrimaryButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            color: Colors.brown,
            child: new Text('Update Property',
                style: new TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: updateProperty,
          ),
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget showImageIcon() {
    return new Container(
      child: new Row(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.fromLTRB(0.0, 0.0, 5.0, 0.0),
              child: new FlatButton(
                child: new Text(imageText,
                    style: new TextStyle(
                        fontSize: 18.0, fontWeight: FontWeight.w300)),
                onPressed: cameraMethod,
              ),
            ),
            new Icon(Icons.camera_alt,
                color: const Color(0xFF000000), size: 30.0)
          ]),
      padding: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      alignment: Alignment.centerRight,
    );
  }

  cameraMethod() async {
    _image = await ImagePicker.pickImage(
      source: ImageSource.camera,
    );

    setState(() {
      _imgURL = _image.toString();
    });
    print(_image.toString());
    uploadFile(file: _image);
  }

  Future<void> uploadFile({File file}) async {
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child('properties/${Path.basename(_image.path)}');
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    uploadTask.events.listen((event) {
      setState(() {
        _isLoading = true;
        _progess = event.snapshot.bytesTransferred.toDouble() *
            100 /
            event.snapshot.totalByteCount.toDouble();
        print(_progess);
        imageText =
            Constants.UPLOADING + " " + _progess.round().toString() + " %";
      });
    }).onError((error) {
      _isLoading = false;
      print('$error');
      setState(() {
        imageText = Constants.IMAGE_ERR;
      });
    });
    await uploadTask.onComplete;
    _isLoading = false;
    print(Constants.FILE_UPLOADED);
    storageReference.getDownloadURL().then((fileURL) {
      print(fileURL);
      setState(() {
        _uploadedFileURL = fileURL;
        _propertyModel.imgUrl = _uploadedFileURL;
        imageText = Constants.FILE_UPLOADED;
      });
    });
  }

  void updateProperty() {
    print(_uploadedFileURL);
    if (validation()) {
      _propertyModel.ownerContact = _phone.text.toString();
      _propertyModel.ownerName = _ownerInfo.text.toString();
      _propertyModel.houseNo = _houseNo.text.toString();
      _propertyModel.houseDescription = _houseDesc.text.toString();
      _propertyModel.streetName = _street.text.toString();
      _propertyModel.address = _address.text.toString();
      _propertyModel.pin = _pin.text.toString();
      _propertyModel.imgUrl = _uploadedFileURL;
      if (_propertyModel != null) {
        _database.reference().child("Properties").child(_propertyModel.key).set(
            _propertyModel.toJson());
        showToast("Property Updated!");
        Navigator.pop(context);
      }else{
        showToast("Something went wrong");
      }
    }
  }

  bool validation() {
    print(!_isLoading);
    if (_ownerInfo.text.length > 2 && _phone.text.length > 8) {
      if (_houseNo.text.length > 4 && _houseDesc.text.length > 4) {
        if (_address.text.length > 5 && _street.text.length > 2) {
          if (_pin.text.length > 3) {
            if (!_isLoading) {
              return true;
            } else {
              showToast("Property picture is uploading");
              return false;
            }
          } else {
            showToast("Enter valid postal code");
            return false;
          }
        } else {
          showToast("Enter valid street name & Address");
          return false;
        }
      } else {
        showToast("Enter valid House Number & Description");
        return false;
      }
    } else {
      showToast("Enter valid Owner Information");
      return false;
    }
  }

  void showToast(String msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
    );
  }
}
