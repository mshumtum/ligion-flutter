import 'dart:async';
import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ligion_auth/Utiles/ActionUtils.dart';
import 'package:ligion_auth/Utiles/Constant.dart';
import 'package:ligion_auth/models/add_property_model.dart';
import 'package:ligion_auth/models/todo.dart';
import 'package:ligion_auth/pages/edit_property.dart';
import 'package:ligion_auth/pages/view_property.dart';
import 'package:ligion_auth/services/authentication.dart';
import 'package:url_launcher/url_launcher.dart';

import 'ImagePreview.dart';
import 'add_new_prop.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);
  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<AddPropertyModel> _PropertyList;
  static GlobalKey _menuKey = new GlobalKey();
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final _textEditingController = TextEditingController();
  StreamSubscription<Event> _onTodoAddedSubscription;
  StreamSubscription<Event> _onTodoChangedSubscription;
  Query _todoQuery;
  String _uId;

  //bool _isEmailVerified = false;

  @override
  void initState() {
    super.initState();

    //_checkEmailVerification();
    _uId = widget.userId;
    _PropertyList = new List();
    _todoQuery = _database
        .reference()
        .child("Properties")
        .orderByChild("userId")
        .equalTo(widget.userId);
    _onTodoAddedSubscription = _todoQuery.onChildAdded.listen(onEntryAdded);
    _onTodoChangedSubscription =
        _todoQuery.onChildChanged.listen(onEntryChanged);
    Constants.USER_ID=widget.userId;
  }

  @override
  void dispose() {
    _onTodoAddedSubscription.cancel();
    _onTodoChangedSubscription.cancel();
    super.dispose();
  }

  onEntryChanged(Event event) {
    var oldEntry = _PropertyList.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });

    setState(() {
      _PropertyList[_PropertyList.indexOf(oldEntry)] =
          AddPropertyModel.fromSnapshot(event.snapshot);
    });
  }

  onEntryAdded(Event event) {
    setState(() {
      _PropertyList.add(AddPropertyModel.fromSnapshot(event.snapshot));
    });
  }

  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }

  addNewTodo(String todoItem) {
    if (todoItem.length > 0) {
      Todo todo = new Todo(todoItem.toString(), widget.userId, false);
      _database.reference().child("todo").push().set(todo.toJson());
    }
  }

  updateTodo(int i) {
    //Toggle completed
    _PropertyList[i].ownerName = "Hello";
    if (_PropertyList != null) {
      _database.reference().child("Properties").child(_PropertyList[i].key).set(_PropertyList[i].toJson());
    }
  }

  deleteTodo(String todoId, int index) {
    _database.reference().child("Properties").child(todoId).remove().then((_) {
      print("Delete $todoId successful");
      setState(() {
        _PropertyList.removeAt(index);
      });
    });
  }

  showAddTodoDialog(BuildContext context) async {
    _textEditingController.clear();
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new Row(
              children: <Widget>[
                new Expanded(
                    child: new TextField(
                      controller: _textEditingController,
                      autofocus: true,
                      decoration: new InputDecoration(
                        labelText: 'Add new todo',
                      ),
                    ))
              ],
            ),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              new FlatButton(
                  child: const Text('Save'),
                  onPressed: () {
                    addNewTodo(_textEditingController.text.toString());
                    Navigator.pop(context);
                  })
            ],
          );
        });
  }

  Widget showTodoList() {
    if (_PropertyList.length > 0) {
      return ListView.builder(
          shrinkWrap: true,
          reverse: true,
          itemCount: _PropertyList.length,
          itemBuilder: (BuildContext context, int index) {
            String todoId = _PropertyList[index].key;
            String ownerContact = _PropertyList[index].ownerContact;
            String houseNo = _PropertyList[index].houseNo;
            String streetName = _PropertyList[index].streetName;
            String imgUrl = _PropertyList[index].imgUrl;
            print(imgUrl);
            String userId = _PropertyList[index].userId;
            return Container(
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                color: Colors.brown[300],
                elevation: 10,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ListTile(
                      leading: Container(
                        width: 120,
                        decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(10.0),
                            image: DecorationImage(
                                image:
                                NetworkImage(_PropertyList[index].imgUrl),
                                fit: BoxFit.fill)),
                      ),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                            builder: (context) => ImagePreview(_PropertyList[index].imgUrl)
                        ));
                      },
                      title:
                      Text(houseNo, style: TextStyle(color: Colors.white)),
                      subtitle: Text(streetName,
                          style: TextStyle(color: Colors.white)),
                      trailing: button(index),
                    ),
                    ButtonTheme.bar(
                      child: ButtonBar(
                        children: <Widget>[
                          FlatButton(
                            child: const Text('View',
                                style: TextStyle(color: Colors.white)),
                            onPressed: () {
                              _launchView(index);
                            },
                          ),
                          FlatButton(
                            child: const Text('Call',
                                style: TextStyle(color: Colors.white)),
                            onPressed: () {
                              _launchCaller(index);
                            },
                          ),
                          FlatButton(
                            child: const Text('MAP',
                                style: TextStyle(color: Colors.white)),
                            onPressed: () {
                              _launchMaps(index);
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            );
            /*    return Dismissible(_launchMaps
              key: Key(todoId),

              child: ListTile(
                title: Text(
                  subject,
                  style: TextStyle(fontSize: 20.0),
                ),
              ),
            );*/
          });
    } else {
      return Center(
          child: Text(
            "Welcome. Property list is empty",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 30.0),
          ));
    }
  }

  button(int i) {
    return new PopupMenuButton(
        itemBuilder: (_) =>
        <PopupMenuItem<String>>[
          new PopupMenuItem<String>(child: const Text('Edit'), value: "1"),
          new PopupMenuItem<String>(
              child: const Text('Delete'), value: "2"),
        ],
        onSelected: (_) {
          print(_);
          if (_ == "1") {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => new EditProperty(),
                  settings: RouteSettings(
                    arguments: _PropertyList[i],
                  )),
            );
          } else {
            deleteTodo(_PropertyList[i].key, i);
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Legion Homepage'),
          actions: <Widget>[
            new FlatButton(
                child: new Text('Logout',
                    style: new TextStyle(fontSize: 15.0, color: Colors.white)),
                onPressed: openSignOutDialog)
          ],
        ),
        body: showTodoList(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            //  showAddTodoDialog(context);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AddNewProp(_uId)),
            );
          },
          tooltip: 'Add Property',
          child: Icon(Icons.home),
        ));
  }

  void _launchMaps(int index) {
    ActionUtils.openMap(double.parse(_PropertyList[index].lat),
        double.parse(_PropertyList[index].lng));
  }

  _launchCaller(int index) async {
    ActionUtils.launchCaller(_PropertyList[index].ownerContact);
  }

  void _launchView(int index) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => new ViewProperty(),
          settings: RouteSettings(
            arguments: _PropertyList[index],
          )),
    );
  }

   openSignOutDialog() async {
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new Container(
              child: Text("Are You Sure ?"),
            ),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              new FlatButton(
                  child: const Text('Logout'),
                  onPressed: () {
                    Navigator.pop(context);
                    signOut();
                  })
            ],
          );
        });
  }
}
