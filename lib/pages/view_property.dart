import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ligion_auth/Utiles/ActionUtils.dart';
import 'package:ligion_auth/Utiles/Constant.dart';
import 'package:ligion_auth/models/add_property_model.dart';
import 'package:share/share.dart';

import 'ImagePreview.dart';

class ViewProperty extends StatelessWidget {
  AddPropertyModel _propertyModel;

  @override
  Widget build(BuildContext context) {
    _propertyModel = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text(Constants.PROPERTY_DETAILS),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () {
              final RenderBox box = context.findRenderObject();
              Share.share("Hello",
                  subject: "subject",
                  sharePositionOrigin:
                  box.localToGlobal(Offset.zero) &
                  box.size);
            },
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          _showForm(context),
        ],
      ),
    );
  }

  Widget _showForm(BuildContext context) {
    return new Container(
      padding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 12.0),
      child: new ListView(
        shrinkWrap: true,
        children: <Widget>[
          showImage(context),
          showHouseName(),
          showStreetName(),
          showAddressPin(),
         /* Expanded(
              child: Divider()
          ),*/
          new Divider(
            color: Colors.grey,
          ),
          showCallMapIcon(),
          new Divider(
            color: Colors.grey,
          ),

         /* Expanded(
              child: Divider()
          ),*/
          showDiscription()
        ],
      ),
    );
  }

  Widget showImage(BuildContext context) {
    return new GestureDetector(
      onTap: (){
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ImagePreview(_propertyModel.imgUrl)
            ));
      },
      child: new Container(
        child: new Image.network(_propertyModel.imgUrl),
        alignment: Alignment.center,
        height: 270.0,
      )
    );
  }

  Widget showOwnerInfo() {
    return new Container(
      child: new Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.fromLTRB(0.0, 0.0, 5.0, 0.0),
              child: new FlatButton(
                child: new Text(_propertyModel.ownerName,
                    style: new TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.black87)),
                onPressed: null,
              ),
            ),
          ]),
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget showHouseName() {
    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.fromLTRB(10.0, 15.0, 0.0, 0.0),
     child: new Text(_propertyModel.houseNo,
            style: new TextStyle(
                fontSize: 19.0,
                fontWeight: FontWeight.w500,
                color: Colors.black87)),
    );
  }

  Widget showStreetName() {
    return Container(
      alignment: Alignment.centerLeft,
      child: new Text(_propertyModel.streetName,
          style: new TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.w400,
              color: Colors.black54)),
      padding: const EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 0.0),
    );
  }

  Widget showAddressPin() {
    return Container(
      alignment: Alignment.centerLeft,
      child: new Text(_propertyModel.address + " - " + _propertyModel.pin,
          style: new TextStyle(
              fontSize: 17.0,
              fontWeight: FontWeight.w300,
              color: Colors.black54)),
      padding: const EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 8.0),
    );
  }

  Widget showCallMapIcon() {
    return new Container(
      child: new Row(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.fromLTRB(0.0, 0.0, 5.0, 0.0),
              child: new FlatButton(
                child: new Text(_propertyModel.ownerName,
                    style: new TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.w300,
                        color: Colors.black87)),
                onPressed: null,
              ),
            ),
            IconButton(
              icon: Icon(Icons.call),
              onPressed: () {
                ActionUtils.launchCaller(_propertyModel.ownerContact);
              },
            ),
            IconButton(
              icon: Icon(Icons.gps_fixed),
              onPressed: () {
                ActionUtils.openMap(double.parse(_propertyModel.lat),
                    double.parse(_propertyModel.lng));
              },
            )
          ]),
      padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget showDiscription() {
    return Container(
      alignment: Alignment.centerLeft,
      child: new Text("Description : \n"+_propertyModel.houseDescription+" \n\n"+"Landmark :",
          style: new TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.w300,
              color: Colors.black)),
      padding: const EdgeInsets.fromLTRB(10.0, 5.0, 0.0, 8.0),
    );
  }
}
