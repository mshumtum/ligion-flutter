import 'dart:async';
import 'dart:io';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ligion_auth/Utiles/Constant.dart';
import 'package:ligion_auth/models/add_property_model.dart';
import 'package:path/path.dart' as Path;

class AddNewProp extends StatefulWidget {
  AddNewProp(this.uId);

  final String uId;

  @override
  _AddNewPropState createState() => _AddNewPropState();
}

class _AddNewPropState extends State<AddNewProp> {
  final _formKey = new GlobalKey<FormState>();
  final FirebaseDatabase _database = FirebaseDatabase.instance;
  Query _todoQuery;
  Position _currentPosition;
  double lng1 = 0.0;
  double lat1 = 0.0;
  double _progess = 0;
  String imageText = Constants.IMAGE_TEXT;
  bool _isLoading;
  List<Marker> allMarkers = [];
  int numOfImg=0;
  final _phone = TextEditingController();
  final _ownerInfo = TextEditingController();
  final _houseNo = TextEditingController();
  final _houseDesc = TextEditingController();
  final _pin = TextEditingController();
  final _street = TextEditingController();
  final _address = TextEditingController();
  final _landmark = TextEditingController();
  String _errorMessage = "";
  String _imgURL = "",_imgURL1 = "",_imgURL2 = "",_imgURL3 = "",_imgURL4 = "";
  String _uploadedFileURL = "";
  File _image,_image1,_image2,_image3,_image4;
  Completer<GoogleMapController> _controller = Completer();
  StorageReference storageReference = FirebaseStorage.instance.ref();

  @override
  void initState() {
    print(widget.uId);
    _todoQuery = _database
        .reference()
        .child("Properties")
        .orderByChild("userId")
        .equalTo(widget.uId);
    _errorMessage = "";
    _isLoading = false;
    locationMethod();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Add New Property")),
      body: Stack(
        children: <Widget>[
          _showForm(context),
          _showCircularProgress(),
          //  showPrimaryButton(),
        ],
      ),
    );
  }

  Widget _showForm(BuildContext context) {
    return new Container(
        padding: EdgeInsets.fromLTRB(16.0, 2.0, 16.0, 12.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            physics: const AlwaysScrollableScrollPhysics(),
            children: <Widget>[
              showDiscription(),
              showOwnerName(),
              showOwnerContact(),
              showHouseNumber(),
              showHouseDiscription(),
              showStreetName(),
              showAddress(),
              showLandmark(),
              ShowImage(),
              ShowImage1(),
              ShowImage2(),
              ShowImage3(),
              ShowImage4(),
              showImageIcon(),
              showPinCode(),
              showMap(),
              showPrimaryButton()
            ],
          ),
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget showDiscription() {
    return new FlatButton(
      child: new Text('Register  Property for Customers',
          style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
    );
  }

  Widget showOwnerContact() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.phone,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Owner Contact Number',
            icon: new Icon(
              Icons.phone,
              color: Colors.grey,
            )),
        controller: _phone,
      ),
    );
  }

  Widget showHouseNumber() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Property Name',
            icon: new Icon(
              Icons.home,
              color: Colors.grey,
            )),
        controller: _houseNo,
      ),
    );
  }

  Widget showHouseDiscription() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Property Description like 3BHK',
            icon: new Icon(
              Icons.home,
              color: Colors.grey,
            )),
        controller: _houseDesc,
      ),
    );
  }

  Widget showOwnerName() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Owner Name',
            icon: new Icon(
              Icons.person_pin,
              color: Colors.grey,
            )),
        controller: _ownerInfo,
      ),
    );
  }

  Widget showStreetName() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Enter Street name',
            icon: new Icon(
              Icons.pin_drop,
              color: Colors.grey,
            )),
        controller: _street,
      ),
    );
  }

  Widget showAddress() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Enter Address',
            icon: new Icon(
              Icons.pin_drop,
              color: Colors.grey,
            )),
        controller: _address,
      ),
    );
  }

  Widget showLandmark() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Landmark',
            icon: new Icon(
              Icons.pin_drop,
              color: Colors.grey,
            )),
        controller: _landmark,
      ),
    );
  }

  Widget showImageIcon() {
    return new Container(
      child: new Row(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Container(
              padding: const EdgeInsets.fromLTRB(0.0, 0.0, 5.0, 0.0),
              child: new FlatButton(
                child: new Text(imageText,
                    style: new TextStyle(
                        fontSize: 18.0, fontWeight: FontWeight.w300)),
                onPressed: cameraMethod,
              ),
            ),
            new Icon(Icons.camera_alt,
                color: const Color(0xFF000000), size: 30.0)
          ]),
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      alignment: Alignment.centerRight,
    );
  }

  Widget ShowImage() {
    if (_imgURL.length > 0 && _imgURL != null) {
      return new Container(
        child: new Image.file(_image),
        padding: const EdgeInsets.all(0.0),
        alignment: Alignment.center,
        height: 300.0,
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }
  Widget ShowImage1() {
    if (_imgURL1.length > 0 && _imgURL1 != null) {
      return new Container(
        child: new Image.file(_image),
        padding: const EdgeInsets.all(0.0),
        alignment: Alignment.center,
        height: 300.0,
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }
  Widget ShowImage2() {
    if (_imgURL2.length > 0 && _imgURL2 != null) {
      return new Container(
        child: new Image.file(_image2),
        padding: const EdgeInsets.all(0.0),
        alignment: Alignment.center,
        height: 300.0,
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }
  Widget ShowImage3() {
    if (_imgURL3.length > 0 && _imgURL3 != null) {
      return new Container(
        child: new Image.file(_image3),
        padding: const EdgeInsets.all(0.0),
        alignment: Alignment.center,
        height: 300.0,
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }
  Widget ShowImage4() {
    if (_imgURL4.length > 0 && _imgURL4 != null) {
      return new Container(
        child: new Image.file(_image4),
        padding: const EdgeInsets.all(0.0),
        alignment: Alignment.center,
        height: 300.0,
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  Widget showPinCode() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: true,
        decoration: new InputDecoration(
            hintText: 'Postal code',
            icon: new Icon(
              Icons.local_post_office,
              color: Colors.grey,
            )),
        controller: _pin,
      ),
    );
  }

  Widget showMap() {
    if (lat1 != 0.0) {
      return new Container(
        child: new GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: LatLng(lat1, lng1),
            zoom: 17.0,
          ),
          markers: Set.from(allMarkers), //values.toSet(),
        ),
        padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
        // alignment: Alignment.center,
        height: 300.0,
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  Widget showPrimaryButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            color: Colors.brown,
            child: new Text('Add Property',
                style: new TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: AddProperty,
          ),
        ));
  }

  cameraMethod() async {
    _image = await ImagePicker.pickImage(
      source: ImageSource.camera,
    );
    //getCameraImage(_image);
    setState(() {

      _imgURL = _image.toString();
    });

    uploadFile(file: _image);
  }

  locationMethod() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        lng1 = _currentPosition.longitude;
        lat1 = _currentPosition.latitude;
        markerfunction(lng1, lat1);
        print(_currentPosition);
        allMarkers.add(Marker(
            markerId: MarkerId('myMarker'),
            draggable: true,
            onTap: () {
              print('Marker Tapped');
            },
            position: LatLng(lat1, lng1)));
      });
    }).catchError((e) {
      print(e);
    });
  }

  void markerfunction(double longitude, double latitude) {
    print("$longitude,$latitude");
    allMarkers.add(Marker(
        markerId: MarkerId('myMarker'),
        draggable: true,
        onTap: () {
          print('Marker Tapped');
        },
        position: LatLng(longitude, latitude)));
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  Future<void> uploadFile({File file}) async {
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child('properties/${Path.basename(_image.path)}');
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    uploadTask.events.listen((event) {
      setState(() {
        _isLoading = true;
        _progess = event.snapshot.bytesTransferred.toDouble() *
            100 /
            event.snapshot.totalByteCount.toDouble();
        print(_progess);
        imageText =
            Constants.UPLOADING + " " + _progess.round().toString() + " %";
      });
    }).onError((error) {
      _isLoading = false;
      print('$error');
      setState(() {
        imageText = Constants.IMAGE_ERR;
      });
    });
    await uploadTask.onComplete;
    _isLoading = false;
    print(Constants.FILE_UPLOADED);
    storageReference.getDownloadURL().then((fileURL) {
      setState(() {
        _uploadedFileURL = fileURL;
        imageText = Constants.FILE_UPLOADED;
      });
    });
  }

  void AddProperty() {
    if (validation()) {
      AddPropertyModel model = new AddPropertyModel(
          _phone.text.toString(),
          _ownerInfo.text.toString(),
          _houseNo.text.toString(),
          _houseDesc.text.toString(),
          _street.text.toString(),
          _address.text.toString(),
          _landmark.text.toString(),
          _uploadedFileURL,
          lat1.toString(),
          lng1.toString(),
          _pin.text.toString(),
          widget.uId);
      _database.reference().child("Properties").push().set(model.toJson());
      showToast("Property Added!");
      Navigator.pop(context);
    }
    print(validation());
  }

  bool validation() {
    if (_ownerInfo.text.length > 2 && _phone.text.length > 8) {
      if (_houseNo.text.length > 4 && _houseDesc.text.length > 4) {
        if (_address.text.length > 5 && _street.text.length > 2) {
          if (_pin.text.length > 3) {
            if (_uploadedFileURL.length != 0) {
              return true;
            } else {
              showToast("Please add property picture");
              return false;
            }
          } else {
            showToast("Enter valid postal code");
            return false;
          }
        } else {
          showToast("Enter valid street name & Address");
          return false;
        }
      } else {
        showToast("Enter valid House Number & Description");
        return false;
      }
    } else {
      showToast("Enter valid Owner Information");
      return false;
    }
  }

  void showToast(String msg) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_LONG,
    );
  }
}
