import 'dart:async';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ligion_auth/pages/root_page.dart';
import 'package:ligion_auth/services/authentication.dart';

class LoginSignupPage extends StatefulWidget {
  LoginSignupPage({this.auth, this.loginCallback});

  final BaseAuth auth;
  final VoidCallback loginCallback;

  @override
  State<StatefulWidget> createState() => new _LoginSignupPageState();
}

class _LoginSignupPageState extends State<LoginSignupPage> {
  final _formKey = new GlobalKey<FormState>();
   bool isOffline = false;

  String _email = "";
  String _password = "";
  String _errorMessage = "";
  final _emaillEditingController = TextEditingController();
  bool _isLoginForm;
  bool _isLoading;

  // Check if form is valid before perform login or signup
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  forgotPassword(String email) async {
    loadEnable("", true);
    //if(_email.length!=0){
    _formKey.currentState.save();
    if (_isLoginForm) {
      // await widget.auth.resetPassword(_email);
      try {
        loadEnable("", false);
        print("ddd saa");
        await widget.auth.sendPasswordResetEmail(email);
        Fluttertoast.showToast(
          msg: "Email Sent! Reset Password",
          toastLength: Toast.LENGTH_LONG,
        );
      } catch (e) {
        print("skkmsksjdnjdnjd");
      }
      // } else {
      //   print("form key is valid");
      // }
    }
  }

  void validation() {
    _formKey.currentState.save();
    print(_email);
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(_email);
    if (emailValid) {
      if (_password.length == 0) {
        loadEnable("Please enter Password", false);
      } else {
        if (_password.length > 5) {
          loadEnable("", true);
          validateAndSubmit();
        } else {
          loadEnable("Please enter more then six character", false);
        }
      }
    } else {
      loadEnable("Enter vaild email", false);
    }
  }

  // Perform login or signup
  void validateAndSubmit() async {
    if (validateAndSave()) {
      String userId = "";
      try {
        if (_isLoginForm) {
          print(_email);
          print(_password);
          // if(_email.ma)
          userId = await widget.auth.signIn(_email, _password);
          widget.loginCallback();
          print('Signed in: $userId');
          if (userId.length > 0 && userId != null && _isLoginForm) {
            loadEnable("", false);
            goIntoHomeScreen();
          } else {
            loadEnable("No user found,Please Signup first", false);
          }
        } else {
          print(_password.length);

          userId = await widget.auth.signUp(_email, _password);
          //widget.auth.sendEmailVerification();
          //_showVerifyEmailSentDialog();
          print('Signed up user: $userId');
          goIntoHomeScreen();
        }
      } catch (e) {
        print('Error: $e');
        loadEnable(e.message, false);
      }
    } else {
      loadEnable("Enter valid fields", false);
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    _isLoginForm = true;

    super.initState();

  }
  void connectionChanged(dynamic hasConnection) {
    print(hasConnection);
    setState(() {
      isOffline = !hasConnection;
    });
  }
  void resetForm() {
    _formKey.currentState.reset();
    _errorMessage = "";
  }

  void toggleFormMode() {
    resetForm();
    setState(() {
      _isLoginForm = !_isLoginForm;
    });
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Legion Real Estate LCC'),
        ),
        body: Stack(
          children: <Widget>[
            _showForm(context),
            _showCircularProgress(),
          ],
        ));
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  //  void _showVerifyEmailSentDialog() {
  //    showDialog(
  //      context: context,
  //      builder: (BuildContext context) {
  //        // return object of type Dialog
  //        return AlertDialog(
  //          title: new Text("Verify your account"),
  //          content:
  //              new Text("Link to verify account has been sent to your email"),
  //          actions: <Widget>[
  //            new FlatButton(
  //              child: new Text("Dismiss"),
  //              onPressed: () {
  //                toggleFormMode();
  //                Navigator.of(context).pop();
  //              },
  //            ),
  //          ],
  //        );
  //      },
  //    );
  //  }

  Widget _showForm(BuildContext context) {
    return new Container(
        padding: EdgeInsets.all(16.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              showLogo(),
              showEmailInput(),
              showPasswordInput(),
              showPrimaryButton(),
              new Padding(
                padding: EdgeInsets.fromLTRB(0.0, 10.0, 20.0, 0.0),
                child: showResetPasswordText(context),
              )
              //
              ,
              showSecondaryButton(),
              showErrorMessage(),
            ],
          ),
        ));
  }

  Widget showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return new Text(
        "",
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  Widget showLogo() {
    return new Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.fromLTRB(0.0, 70.0, 0.0, 0.0),
        child: CircleAvatar(
          backgroundColor: Colors.transparent,
          radius: 48.0,
          child: Image.asset('images/icon.png'),
        ),
      ),
    );
  }

  Widget showEmailInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 100.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Email',
            icon: new Icon(
              Icons.mail,
              color: Colors.grey,
            )),
        validator: (value) => value.isEmpty ? 'Email can\'t be empty' : null,
        onSaved: (value) => _email = value.trim(),
      ),
    );
  }

  Widget showPasswordInput() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        obscureText: true,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Password',
            icon: new Icon(
              Icons.lock,
              color: Colors.grey,
            )),
        validator: (value) => value.isEmpty ? 'Password can\'t be empty' : null,
        onSaved: (value) => _password = value.trim(),
      ),
    );
  }

  Widget showSecondaryButton() {
    return new FlatButton(
        child: new Text(
            _isLoginForm ? 'Create an account' : 'Have an account? Sign in',
            style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300)),
        onPressed: toggleFormMode);
  }

  Widget showPrimaryButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0)),
            color: Colors.brown,
            child: new Text(_isLoginForm ? 'Login' : 'Create account',
                style: new TextStyle(fontSize: 20.0, color: Colors.white)),
            onPressed: validation,
          ),
        ));
  }

  Widget showResetPasswordText(BuildContext context) {
    return new Column(
      children: <Widget>[
        Align(
          alignment: Alignment.bottomRight,
          child: FlatButton(
            child: Text(_isLoginForm ? 'Forgot Password' : ''),
            onPressed: () {
              openForgotDialog(context);
            },
          ),
        ),
      ],
    );
  }

  openForgotDialog(BuildContext context) async {
    _emaillEditingController.clear();
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new Row(
              children: <Widget>[
                new Expanded(
                    child: new TextField(
                  controller: _emaillEditingController,
                  autofocus: true,
                  decoration: new InputDecoration(
                    labelText: 'Enter Email for Forgot Password',
                  ),
                ))
              ],
            ),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              new FlatButton(
                  child: const Text('Submit'),
                  onPressed: () {
                    forgotPassword(_emaillEditingController.text.toString());
                    Navigator.pop(context);
                  })
            ],
          );
        });
  }

  void callErrorMethod() {
    setState(() {
      _errorMessage = "Auth Error";
    });
  }

  void loadEnable(String msg, bool load) {
    setState(() {
      _errorMessage = msg;
      _isLoading = load;
    });
    if (msg == "") {
    } else {
      Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_LONG,
      );
    }
  }

  void goIntoHomeScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => new RootPage(auth: new Auth())),
    );
  }
}
